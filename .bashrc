# Check for an interactive session
[ -z "$PS1" ] && return

export PATH=$PATH:~/bin

alias gomode="sudo su"
alias ls='ls --color=auto'
alias cp='cp -v'

#alias ls="ls -G" # on osx
alias start_ubuntu='VBoxManage startvm "ubuntu dev"'
alias start_arch='VBoxManage startvm "arch"'

export PYTHONPATH=$PYTHONPATH:./
export PATH=$PATH:~/bin

alias gomode="sudo su"
alias start_virtual_linux='VBoxManage startvm "ubuntu dev"'


alias tree='tree -Csu'

alias webshare='python -m SimpleHTTPServer'

export EDITOR=vim

function parse_git_branch {
git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

function promptl {
local        BLUE="\[\033[0;34m\]"
local         RED="\[\033[0;31m\]"
local   LIGHT_RED="\[\033[1;31m\]"
local       GREEN="\[\033[0;32m\]"
local LIGHT_GREEN="\[\033[1;32m\]"
local       WHITE="\[\033[1;37m\]"
local  LIGHT_GRAY="\[\033[0;37m\]"
case $TERM in
xterm*)
TITLEBAR='\[\033]0;\u@\h:\W\007\]'
;;
*)
TITLEBAR=""
;;
esac

PS1="${TITLEBAR}\
$BLUE[$RED\W$LIGHT_GRAY:$GREEN\$(parse_git_branch)$BLUE]\
$GREEN\$ $LIGHT_GRAY"
PS2='> '
PS4='+ '
}
promptl

